from django.db import models

# Create your models here.
from django.core.validators import RegexValidator

class Donacion(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return '{}'.format(self.name)
class Servicio(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return '{}'.format(self.name)

class Adopcion(models.Model):
    key = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, unique=True)
    nit = models.IntegerField(blank=False)
    phone = models.IntegerField(blank=False)
    email = models.EmailField(max_length=150)
    addres = models.TextField(max_length=300)

    def __str__(self):
        return (self.name)
class Usuario(models.Model):
    phone_regex = RegexValidator(
        regex=r'^\d{7,11}$', message="Phone number must be entered in the format: '99999999'")
    SERVICES = [(1, 'donacion'), (2, 'servicio comunitario')]
    name = models.CharField(max_length=30)
    lastName = models.CharField(max_length=50)
    email = models.EmailField(max_length=50, unique=True)
    phone = models.CharField(
        validators=[phone_regex], unique=True, max_length=11, blank=True)
    # service = models.IntegerField(
    #     choices=SERVICES,
    #     default=1,
    # )
    servicio = models.ManyToManyField(Servicio, blank=True)
    donacion = models.ManyToManyField(Donacion, blank=True)
    adopcion = models.ManyToManyField(Adopcion, blank=True)

    #donacion= models.ForeignKey(Donacion, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {}'.format(self.name, self.lastName)
