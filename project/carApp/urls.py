from django.urls import path
from carApp.views import say_hi

# Create your views here.
urlpatterns=[
    path('hi', say_hi)
]