from django.contrib import admin
from .models import Usuario, Servicio, Donacion, Adopcion
# Register your models here.


class UserAdmin(admin.ModelAdmin):
    list_display = ('name', 'lastName', 'email')


admin.site.register(Usuario, UserAdmin)
admin.site.register(Adopcion)
admin.site.register(Servicio)
admin.site.register(Donacion)



