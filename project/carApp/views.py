import json
from django.http import  HttpResponse
from carApp.models import Usuario
from django.shortcuts import render

# Create your views here.
''' 
#template lista easy
def say_hi(request):
    user= Usuario.objects.all()
    cadena = 'Users CanApp List'
    cadena +='<br><ul>'

    for users in user:
        cadena += '<li>Name: {0} </li>'.format(users.name)
        cadena += 'Lastname: {0}  '.format(users.lastName)
        cadena += ', '
        cadena += 'Phone: {0} '.format(users.phone)
        cadena += ', '
        cadena += 'Email: {0} '.format(users.email)
        cadena += ', '
        cadena += 'Servicio: {0} '.format(users.servicio)
        
    cadena += '</ul>'
    return HttpResponse(cadena)
'''
''' 
# Formato Json
def say_hi(request):
    users= list(Usuario.objects.all().values('id', 'name', 'lastName'))
    return HttpResponse(json.dumps(users), content_type='application/json')
'''

def say_hi(request):
    users = list(Usuario.objects.all().values('id', 'name', 'lastName', 'phone', 'email','servicio'))
    context = {
        'users': users
    }
    return render(request, 'test.html', context)