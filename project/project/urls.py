from django.contrib import admin
from django.urls import path, include
from project.views import index
from project.views import login

urlpatterns = [
    path('admin/', admin.site.urls),
    path('index', index),
    path('login/', login),
    path('', include('carApp.urls')),
]
